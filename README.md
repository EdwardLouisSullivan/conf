# Home directory git config syncing.

Thanks to Alec Heller (See https://gitlab.com/alec1/conf) for
teaching me this trick.


## To bootstrap the first time only

`git clone --bare git@bitbucket.org:EdwardLouisSullivan/conf.git ~/.conf`

`git --work-tree=$HOME --git-dir=$HOME/.conf checkout HEAD`


## To use

Make sure `~/bin` is in `$PATH`

`chmod +x ~/bin/conf`

Use the `conf` command as an alias to git and go to town


## Example usage

`conf pull`

`conf add example.txt`

`conf commit -m 'Adding an example file'`

`conf push`



## The conf script (which goes in ~/bin)
```
#!/bin/sh
GIT_DIR=$HOME/.conf
export GIT_DIR
GIT_WORK_TREE=$HOME
export GIT_WORK_TREE
if [ ! -d $GIT_DIR ]; then
  mkdir -p $GIT_DIR;
fi
exec git "$@"
```


## Make sure the config for my config repo is correct

The `~/.git/config` file should look like this. The
most important part is the line `fetch = +refs/heads/*:refs/remotes/origin/*`.

For some reason, that line is occaisionally missing,
possibly because of the bare init/clone. There is no
elegant way to automatically include the `~/.git/config`
file in the repo, as far as I understand.

Some of the lines (logallrefupdates and the entire
branch section) might not matter.
```
[core]
  repositoryformatversion = 0
  filemode = true
  bare = true
  logallrefupdates = true
  ignorecase = true
  precomposeunicode = true

[remote "origin"]
  url = git@bitbucket.org:EdwardLouisSullivan/conf.git
  fetch = +refs/heads/*:refs/remotes/origin/*

[branch "master"]
  remote = origin
  merge = refs/heads/master
```
Typically, an install with `git clone git@bitbucket.org:EdwardLouisSullivan/conf.git`
will include the correct remote configuration, but for this repo, you need
to do a bare install, and that might mess up the remote configuration.


## Notes

Sometimes, applications do not have convenient config files, and instead the gui is the only practical way to changes their settings. I keep a separate list of those settings to change by hand, stored in ~/.HAND.md



