Change the settings for these applications by hand,
since there is no good conf file for them.

## Make sure the config for my config repo is correct

The `~/.git/config` file should look like this. The
most important part is the line `fetch = +refs/heads/*:refs/remotes/origin/*`.

For some reason, that line is occaisionally missing,
possibly because of the bare init/clone. There is no
elegant way to automatically include the `~/.git/config`
file in the repo, as far as I understand.

Some of the lines (logallrefupdates and the entire
branch section) might not matter.
```
[core]
  repositoryformatversion = 0
  filemode = true
  bare = true
  logallrefupdates = true
  ignorecase = true
  precomposeunicode = true

[remote "origin"]
  url = git@bitbucket.org:EdwardLouisSullivan/conf.git
  fetch = +refs/heads/*:refs/remotes/origin/*

[branch "master"]
  remote = origin
  merge = refs/heads/master
```
Typically, an install with `git clone git@bitbucket.org:EdwardLouisSullivan/conf.git`
will include the correct remote configuration, but for this repo, you need
to do a bare install, and that might mess up the remote configuration.


## Install oh-my-z-sh

After installing it, edit ~/.zshrc and point it to my '~/.bash_profile'

if [ -f ~/.bash_profile ]; then
   source ~/.bash_profile
fi

## If setting up a Mac for the first time, set a good username and hostname

By default, I think the Mac setup creates a user account by
asking for your full name, then concatenating the pieces
in lowercase. Ensure that it creates a short
username that you'll be satisfied with, because otherwise it's a pain in the
ass to change the username afterwards without creating an
entirely new account.

You can easily change the hostname for a Mac anytime. Go to
`System Preferences` -> `Sharing` -> `Computer name`

## Set basic OSX settings

Enable tapping the track pad to "click"

Undo the default setting of the "natural scroll direction"

Increase the default trackpad speed from 4 to 8

Click the battery icon in the top menu bar to `Show Percentage`

Disable showing the content of text message in notifications
from the Messages app.
1. `System Preferences` -> `Notifications` -> `Messages`
-> unselect `Show message preview`

Hide the dock when you are not using it. Select `System Preferencess` ->
`Dock` -> `Automatically hide and show the Dock`.

Open the finder, go to preferences, and set options:
- Keep folders on top when sorting by name
- Show all filename extensions
- On the finder preferences submenu for the sidebar, select all checkboxes except
the hostname

For .mht files, set the default application to chrome. To do this,
right click a .mht file and click "Get Info", then change the
default application to chrome and select the button that says
"Change all."

Make sure that Mac (or ipad) can actually get forwarded imessages from
my iphone.

If I have a mouse, enable right clicking.

In some (not all) applications, the OSx will not allow holding down a key
to repeat that key. For example, in VS Code, holding down an
alphabetic code simply presents the user with the option to
type an alternate charact. To enable key repetition for all
applications, in the terminal, type:
`defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false`
According to http://www.idownloadblog.com/2015/01/14/how-to-enable-key-repeats-on-your-mac/

Allow good zoom-in:
- `System Preferences` -> `Zoom` -> Set `Use scroll gesture with modifier keys to zoom`
- Set `Command` as the modifier key

Install a newer version of gnu screen, so that vertical splits are possible. Most be greater than 4.01. Just do `brew install screen`, `which screen`, and potentially edit the PATHin `~/.bashrc`

Give ethernet precedence over wireless in network settings on osx.

## On the Touchbar Mac, turn the capslock button in an escape button

The 2017 touchbar Macs doe not have a physical escape button
so I am remapping my seldom-used capslock button into an
escape button. 
1. `System Preferences` -> `Keyboard` icon` -> stay on `Keyboard` tab
-> hit the `Modifier Keys` button in the bottom right 
-> remap the `Caps Lock Key` to the `Escape` function

## Turn on OSX security settings

Enable locking the laptop with a single keyboard shortcut
1. `System Preferences` -> `Security & Privacy` -> `General` tab
-> click `Require password "immediately"`
2. (Optional) `System Preferences` -> `Energy Saver`
-> `Battery` and `Power Adapter` -> lengthen the `Computer sleep` time

Turn on disk encryption
1. `System Preferences` -> `Security & Privacy` -> `FireVault`

Turn on Firewall
1. `System Preferences` -> `Security & Privacy` -> `Firewall`

Set privacy settings
1. `System Preferences` -> `Security & Privacy` -> `Privacy`
-> `Location Services` -> disable all apps but keep `Enable Location Services`
2. `System Preferences` -> `Security & Privacy` -> `Privacy`
-> `Analytics` -> disable all selectboxes

Set the lock-screen message to "This laptop belongs to ... Cell phone: ...
Email: ..."

## Setup brew and your ssh private key and setup ssh agent

Install brew. There is a one-liner on the homepage for brew.

Place your private key in ~/.ssh/id_rsa (or create a new one).

Run `ssh-agent` and copy the top two lines to ~/bin/setupsshagent. Then
run `ssh-add`. In every open screen window or iterm session, run
`source ~/bin/setupsshagent`.

TODO: add notes here about ssh configuration options (forwarding, etc).

## Install zsh and set it as your default shell

After installing zsh, run (without root) `chsh -s /bin/zsh`

## Set keyboard shortcut in OSX for "Paste and Match Style"

https://www.macobserver.com/tmo/article/something-copy-paste

1. Go to `system preferences` -> `keyboard` -> `shortcuts`
-> `App shortcuts`
2. Then click the plus button to add a new shortcut
3. Select `All Applications`
4. Type Menu Title: `Paste and Match Style`
5. Type the shortcut. I prefer `shift, command, V`

Note: you can add multiple menu titles for the same shortcut.
Some applications use the phrase `Paste and Match Style` (e.g. stickies),
but other use `Paste and Match Formatting` (e.g. One Note)

## Set the git ignore for your conf repo

https://stackoverflow.com/questions/6626136/best-practice-for-adding-gitignore-to-repo

Add an asterisk on its own line to: `~/.conf/info/exclude`

## Config custom git settings, and git name and email

Edit the main ~/.gitconfig file to include my custom .gitconfig_ed file.
Specifically, add this to ~/.gitconfig:

[include]
    path = gitconfig_ed

The desired name and email might vary by machine/repo, so
I do not save this config

https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup

`git config --global user.name "John Doe"`
`git config --global user.email johndoe@example.com`

## Install Google Chrome

1. Install Chrome
2. Set the default browser to Chrome. Go to `System Preferences` ->
`General` -> `Default web browser`
3. Optionally log into Chrome
4. If logged into Chrome, configure Chrome to not ask about saving
passwords. Go to `Chrome` -> `Preferences` -> `Advanced` (at bottom)` ->
`Passwords and Forms` -> `Manage passwords` -> turn it off

In gmail, make sure that I've turned off images in emails.

## Install my favorite vscode extensions. Instructions and list here:

  .vscode/extensions.json

  Since vscode's settings are in json, which doesn't support
  comments, I am noting here that I may want to consider
  the following option:

  `"python.linting.pylintArgs": ["--errors-only"]`

## Make sure that your iTerm2 config is being read properly

/Users/edward/Library/Application Support/iTerm2/DynamicProfiles
Note: each dynamic profile must have a different Globally Unique ID
Link about dynamic profiles: https://www.iterm2.com/documentation-dynamic-profiles.html
Note: Dynamic profiles are json, so no comments in the config.
Note: I set the left option button be ESC+/meta so that I could have a keyboard
shortcut for moving forward and backward on word in bash.
(http://teohm.com/blog/shortcuts-to-move-faster-in-bash-command-line/)

When you start iTerm2 for the first time, you should go to the
preferences -> profiles and set my dynamic profile as the default
profile. Note that these settings only apply to the "profile"
settings, which are a subset of the iTerm2 settings. There is
a totally separate way to change the other non-color settings
via loading a plist file (see the bottom of preferences -> general).

## Install Spectacle and set its auto-start option

The config file should take care of most of Spectacle's settings,
but the config file does not save the option to have Spectacle
automatically start when I log into the computer.

Click the Spectacle icon on the top right of the screen and
select Preferences. In the bottom left of the menu, select the
checkbox labeled "Launch Spectacle at login".

Also, set the spectacle keystroke for full screen (command,
option, control, up arrow)

## Setup a cloud backup for repos

Can't use google drive because it does not have a regex-based exclude function

Use MEGAsync. You can use the GUI to add files to exclude, but I'm not sure
where it stores the config information. So, I just add the gitignore names
from one of my repos to the MEGAsync gui by hand.

## Set the Keepassx security settings

About 20 seconds for clipboard clearing.

About 20 minutes (1200 seconds) for locking the database.


## Configure Docker

By default, Docker on Mac only gets 2 GB of RAM and 1 GB
of swap space. Increase that to something like 8 GB. Otherwise,
Docker might crash with cryptic messages that just say "killed"
with no other information.

## Programs to install

1. Homebrew
2. Spectacle
3. VS Code
4. Vim
5. Chrome
6. Google drive
7. Keepassx 
8. Notion
9. PSQL, pgadmin, postgres standalone
10. Spotify
11. Postmas
12. Slack
13. git
14. nodejs, npm, gulp
15. VPN client (Forticlient, Cisco Anyconnect)
16. Kies
17. Dash
18. Jetbrains editors
19. Skype, Zoom.us
20. Toggl timer
21. TeamViewer or Remote Desktop Connection for Mac
22. Steam
23. Tor
24. uTorrent
25. Ventrilo
26. Virtualbox
27. Gimp
28. Microsoft Office
29. GNU Screen
30. MEGAsync
31. pstree (brew install pstree), since ps auxf does not work on mac (-f not available)
32. OmniDiskSweeper

