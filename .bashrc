# echo "Running bashrc"

# https://unix.stackexchange.com/questions/140251/strip-color-on-os-x-with-bsd-sed-or-any-other-tool
remove_color () {
  sed "s,$(printf '\033')\\[[0-9;]*[a-zA-Z],,g"
}

dated () {
  date "+%Y%m%d_%H%M%S"
}

escape_single_quotes () {
  sed -e 's='\''=\\'\''=g'
}

alias mv="mv -i"
export PATH=~/bin:$PATH
export LESS=-Rj.25
export PAGER=less # For psql
export PATH="/Users/ed/Library/Python/2.7/bin/:$PATH"
source setupsshagent

alias glagd="git log --all --graph --decorate"

# Make sure to use a version of screen greater than 4.01 to allow vertical splitting
# brew install screen
export PATH="/usr/local/Cellar/screen/4.6.2/bin/screen:$PATH"

# Make VS-code launchable from the command line
export PATH="/Applications/Visual Studio Code.app/Contents/Resources/app/bin:$PATH"

# Python virtualenvwrapper stuff
#export WORKON_HOME=$HOME/.virtualenvs
#export PROJECT_HOME=$HOME/Devel
#source /usr/local/bin/virtualenvwrapper.sh


# echo "Running ed's old zshrc"
# https://superuser.com/questions/169920/binding-fn-delete-in-zsh-on-mac-os-x
bindkey "^[[3~" delete-char
# https://unix.stackexchange.com/questions/273861/unlimited-history-in-zsh
setopt INC_APPEND_HISTORY
HISTSIZE=10000000
SAVEHIST=10000000
# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[[ -f /Users/edwardsul/.nvm/versions/node/v6.10.3/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh ]] && . /Users/edwardsul/.nvm/versions/node/v6.10.3/lib/node_modules/serverless/node_modules/tabtab/.completions/serverless.zsh
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[[ -f /Users/edwardsul/.nvm/versions/node/v6.10.3/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh ]] && . /Users/edwardsul/.nvm/versions/node/v6.10.3/lib/node_modules/serverless/node_modules/tabtab/.completions/sls.zsh
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"


if [ -f ~/.bash_local ]; then
   source ~/.bash_local
fi
