# echo "Running zshrc"

# https://superuser.com/questions/169920/binding-fn-delete-in-zsh-on-mac-os-x
bindkey "^[[3~" delete-char

# https://unix.stackexchange.com/questions/273861/unlimited-history-in-zsh
setopt INC_APPEND_HISTORY
HISTSIZE=10000000
SAVEHIST=10000000

if [ -f ~/.bash_profile ]; then
   source ~/.bash_profile
fi
