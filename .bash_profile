# echo "Running bash_profile"

if [ -f ~/.bashrc ]; then
   source ~/.bashrc
fi



export FQ_EMAIL="edward.sullivan@floqast.com"
#export NODE_ENV="development"
#export RUN_ENV="local"
export NODE_ENV="development"
export RUN_ENV="local"

export API_AWS_SQS_BI_QUEUE_NAME="api-aws-sqs-bi-edward.fifo"
export API_AWS_SQS_PROGRESSEMAIL_QUEUE_NAME="progress-email-edward.fifo"
export API_AWS_SQS_READYFORREVIEWEMAIL_QUEUE_NAME="ready-for-review-edward.fifo"
export API_AWS_SQS_RUNRECS_INTACCT_QUEUE_NAME="run-recs-sqs-intacct-edward.fifo"
export API_AWS_SQS_RUNRECS_NETSUITE_QUEUE_NAME="run-recs-sqs-netsuite-edward.fifo"
export API_AWS_SQS_RUNRECS_REPORT_QUEUE_NAME="run-recs-report-edward.fifo"
export API_AWS_SQS_RUNRECS_TB_QUEUE_NAME="run-recs-sqs-tb-edward.fifo"
export API_AWS_SQS_RUNRECS_YTD_INTACCT_QUEUE_NAME="run-recs-ytd-intacct-edward.fifo"
export API_AWS_SQS_RUNRECS_YTD_NETSUITE_QUEUE_NAME="run-recs-ytd-netsuite-edward.fifo"
export API_AWS_SQS_RUNRECS_YTD_TB_QUEUE_NAME="run-recs-ytd-tb-edward.fifo"
export BI_SQS_QUEUE_NAME="bi-development-edward.fifo"
export RUN_RECS_SQS_QUEUE_NAME="run-recs-edward.fifo"


# export NVM_DIR="$HOME/.nvm"
# . "/usr/local/opt/nvm/nvm.sh"

export RUN_RECS_NOTIFICATION_EMAIL="${FQ_EMAIL}"
export CRON_UPDATE_TLC_EMAIL_NOTIFICATION="${FQ_EMAIL}"
export SERVER_EMAIL_SETUP="${FQ_EMAIL}"
export SERVER_EMAIL_NOTIFICATION_RUNRECS="${FQ_EMAIL}"
export SERVER_EMAIL_NOTIFICATION_CRON_UPDATETLC="${FQ_EMAIL}"



