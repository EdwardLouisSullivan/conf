colorscheme peachpuff
set backspace=indent,eol,start

" For improving the syntax highlighting color
" Set colors for Visual mode and Search mode highlighting
" https://stackoverflow.com/questions/7103173/vim-how-to-change-the-highlight-color-for-search-hits-and-quickfix-selection

hi Visual ctermfg=lightblue ctermbg=black
hi Search ctermfg=lightgreen ctermbg=black

" Highlight the search results
" https://stackoverflow.com/questions/3431184/highlight-all-occurrence-of-a-selected-word
" Use :noh while using vim stop highlighting the previous search

set hlsearch

" show existing tab with 4 spaces width
set tabstop=2
" when indenting with '>', use 4 spaces width
set shiftwidth=2  
" On pressing tab, insert 4 spaces
set expandtab

:set ruler
syntax on

" https://stackoverflow.com/questions/39816125/vim-text-edtitor-is-it-possible-to-adjust-the-cursor-blink-rate
:set guicursor=i:blinkwait700-blinkon400-blinkoff250

" Remapping keystroke mappings in vim
" https://stackoverflow.com/questions/3776117/what-is-the-difference-between-the-remap-noremap-nnoremap-and-vnoremap-mapping
" http://learnvimscriptthehardway.stevelosh.com/chapters/06.html
:let mapleader = ","
:nnoremap <leader>d "_d
:vnoremap <leader>d "_d
:nnoremap <leader>x "_x
:vnoremap <leader>x "_x
